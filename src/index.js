import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import GlobalStyles from "./styles/global";

const wrapper = document.getElementById("app");

wrapper &&
  ReactDOM.render(
    <>
      <GlobalStyles />
      <App />
    </>,
    wrapper
  );
