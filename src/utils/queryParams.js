export const getQueryParams = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const tripId = urlParams.get("trip");

  if (isNaN(tripId)) {
    window.history.pushState("", "", "/");

    return { tripId: null };
  }

  return { tripId };
};

export const updateTripParam = (nextTripId) => {
  const tripParam = `?trip=${nextTripId}`;

  window.history.pushState("", "", `${tripParam}`);
};
