export function formatDate(timestamp) {
  const date = new Date(timestamp);

  return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
}

export function formatTime(timestamp) {
  const date = new Date(timestamp);

  return date.toLocaleTimeString("es-ES", {
    hour: "2-digit",
    minute: "2-digit",
  });
}
