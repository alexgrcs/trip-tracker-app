import { createGlobalStyle } from "styled-components";
import reset from "./reset";

const globalStyle = createGlobalStyle`
  ${reset}

  html,
  body,
  #app {
    width: 100%;
    height: 100%;
    overflow: hidden;

    @media (max-width: 767px) {
      height: auto;
      overflow: auto;
    }
  }

  body {
    font-size: 1rem;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    margin: 0;
    color: #707070;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .icon {
    display: inline-block;
    width: 1em;
    height: 1em;
    stroke-width: 0;
    stroke: currentColor;
    fill: currentColor;
  }
`;

export default globalStyle;
