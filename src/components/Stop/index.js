import React from "react";
import PropTypes from "prop-types";
import FlagIcon from "./icons/FlagIcon";
import Tooltip from "./components/Tooltip";
import { StopWrapper } from "./styles";
import { tooltipType } from "./types";

function Stop({ lat, lng, onClick, tooltip }) {
  return (
    <StopWrapper onClick={onClick}>
      {tooltip && <Tooltip tooltip={tooltip} />}
      <FlagIcon />
    </StopWrapper>
  );
}

Stop.propTypes = {
  lat: PropTypes.number,
  lng: PropTypes.number,
  onClick: PropTypes.func,
  tooltip: PropTypes.shape(tooltipType),
};

export default Stop;
