import PropTypes from "prop-types";

export const stopType = {
  address: PropTypes.string,
  userName: PropTypes.string,
  paid: PropTypes.bool,
  price: PropTypes.number,
  stopTime: PropTypes.string,
};

export const tooltipType = {
  stop: PropTypes.shape(stopType),
  error: PropTypes.string,
  isFetching: PropTypes.bool,
};
