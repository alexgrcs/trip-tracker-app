import React from "react";
import { render, cleanup } from "@testing-library/react";
import Stop from "./";
import stopData from "../../__mocks__/stopData";

afterEach(cleanup);

it("renders with tooltip component", () => {
  const mockTooltip = { stop: stopData };

  const { asFragment } = render(<Stop tooltip={mockTooltip} />);

  expect(asFragment()).toMatchSnapshot();
});

it("renders without tooltip component", () => {
  const { asFragment } = render(<Stop />);

  expect(asFragment()).toMatchSnapshot();
});
