import React from "react";
import PropTypes from "prop-types";
import { tooltipType } from "../../types";
import { formatDate, formatTime } from "../../../../utils/formatTimestamp";
import TimeIcon from "../../icons/TimeIcon";
import MarkerIcon from "../../icons/MarkerIcon";
import UserIcon from "../../icons/UserIcon";
import PriceIcon from "../../icons/PriceIcon";
import { TooltipWrapper } from "./styles";

function Tooltip({ tooltip }) {
  const { stop, error, isFetching } = tooltip;

  if (isFetching) {
    return (
      <TooltipWrapper>
        <span>Loading...</span>
      </TooltipWrapper>
    );
  }

  if (error) {
    return (
      <TooltipWrapper>
        <span>Sorry, there's no data available :(</span>
      </TooltipWrapper>
    );
  }

  const { address, userName, paid, price, stopTime } = stop;

  return (
    <TooltipWrapper>
      {stopTime && (
        <div>
          <TimeIcon />
          <span>{`${formatDate(stopTime)} – ${formatTime(stopTime)}`}</span>
        </div>
      )}
      {address && (
        <div>
          <MarkerIcon />
          <span>{address}</span>
        </div>
      )}
      {userName && (
        <div>
          <UserIcon />
          <span>{userName}</span>
        </div>
      )}
      {price && (
        <div>
          <PriceIcon />
          <span>{`${price} – ${paid ? "Paid" : "Not paid"}`}</span>
        </div>
      )}
    </TooltipWrapper>
  );
}

Tooltip.propTypes = {
  tooltip: PropTypes.shape(tooltipType),
};

export default Tooltip;
