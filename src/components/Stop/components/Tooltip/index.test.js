import React from "react";
import { render, cleanup } from "@testing-library/react";
import Tooltip from "./";
import stopData from "../../../../__mocks__/stopData";

afterEach(cleanup);

it("renders component with data", () => {
  const mockTooltip = { stop: stopData };

  const { asFragment } = render(<Tooltip tooltip={mockTooltip} />);

  expect(asFragment()).toMatchSnapshot();
});

it("renders component with loading message", () => {
  const mockTooltip = { isFetching: true };

  const { asFragment } = render(<Tooltip tooltip={mockTooltip} />);

  expect(asFragment()).toMatchSnapshot();
});

it("renders component with error message", () => {
  const mockTooltip = { error: "An error message" };

  const { asFragment } = render(<Tooltip tooltip={mockTooltip} />);

  expect(asFragment()).toMatchSnapshot();
});
