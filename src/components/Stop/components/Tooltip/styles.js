import styled from "styled-components";

export const StopWrapper = styled.div`
  position: relative;
`;

export const TooltipWrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  bottom: 75px;
  left: -50px;
  z-index: 5;
  width: 180px;
  min-height: 6em;
  font-size: 0.8rem;
  padding: 1em 1em 1em 0.4em;
  border-radius: 6px;
  background-color: #fff;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.15);

  &:after {
    display: block;
    content: "";
    position: absolute;
    bottom: -10px;
    left: 40px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 10px 10px 0 10px;
    border-color: #ffffff transparent transparent transparent;
  }

  & > div {
    display: flex;
    align-items: center;
    margin-bottom: 0.6em;

    &:last-of-type {
      margin-bottom: 0;
    }
  }

  span {
    flex-basis: calc(100% - 1.8em);
  }

  & > span {
    padding: 0 0.6em;
  }

  svg {
    flex-basis: 1.8em;
    width: 1.8em;
    height: 1em;
    margin-right: 0.4em;
    fill: #707070;
  }
`;
