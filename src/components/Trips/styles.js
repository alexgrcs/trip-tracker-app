import styled from "styled-components";

export const TripsWrapper = styled.div`
  flex-basis: 300px;
  position: relative;
  z-index: 2;
  height: 100vh;
  overflow: hidden;
  box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1);
  & > ul {
    height: 100%;
    margin: 0;
    padding: 0;
    list-style: none;
    overflow: auto;
  }

  @media (max-width: 767px) {
    flex-basis: auto;
    height: auto;
    overflow: auto;

    & > ul {
      height: auto;
      overflow: auto;
    }
  }
`;

export const ErrorWrapper = styled.p`
  padding: 1em;
`;
