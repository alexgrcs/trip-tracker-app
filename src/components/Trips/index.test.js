import React from "react";
import { render, cleanup } from "@testing-library/react";
import Trips from "./";
import tripsData from "../../__mocks__/tripsData";

afterEach(cleanup);

it("renders component with items", () => {
  const mockTrips = tripsData;

  const { asFragment } = render(<Trips trips={mockTrips} />);

  expect(asFragment()).toMatchSnapshot();
});

it("renders component with loader", () => {
  const mockTrips = tripsData;
  const mockIsFetching = true;

  const { asFragment } = render(
    <Trips trips={mockTrips} isFetching={mockIsFetching} />
  );

  expect(asFragment()).toMatchSnapshot();
});

it("renders component with loader", () => {
  const mockTrips = tripsData;
  const mockIsFetching = true;

  const { asFragment } = render(
    <Trips trips={mockTrips} isFetching={mockIsFetching} />
  );

  expect(asFragment()).toMatchSnapshot();
});

it("renders component with error message", () => {
  const mockError = "An error message";

  const { asFragment } = render(<Trips error={mockError} />);

  expect(asFragment()).toMatchSnapshot();
});
