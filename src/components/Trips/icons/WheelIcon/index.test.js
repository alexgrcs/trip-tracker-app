import React from "react";
import { render, cleanup } from "@testing-library/react";
import WheelIcon from "./";

afterEach(cleanup);

it("renders", () => {
  const { asFragment } = render(<WheelIcon />);
  expect(asFragment()).toMatchSnapshot();
});
