import styled from "styled-components";

export const SvgWrapper = styled.svg`
  display: block;
  width: 30px;
  height: 30px;
  margin-top: -1em;
  margin-bottom: 0.5em;
  fill: rgba(0, 0, 0, 0.2);
  opacity: ${(props) => (props.isActive ? 1 : 0)};
  transition: opacity 0.25 ease-out;
`;
