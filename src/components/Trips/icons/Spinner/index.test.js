import React from "react";
import { render, cleanup } from "@testing-library/react";
import Spinner from "./";

afterEach(cleanup);

it("renders", () => {
  const { asFragment } = render(<Spinner />);
  expect(asFragment()).toMatchSnapshot();
});
