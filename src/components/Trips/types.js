import PropTypes from "prop-types";

export const tripType = {
  description: PropTypes.string,
  destination: PropTypes.shape({
    address: PropTypes.string,
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  driverName: PropTypes.string,
  endTime: PropTypes.string,
  origin: PropTypes.shape({
    address: PropTypes.string,
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  route: PropTypes.string,
  startTime: PropTypes.string,
  status: PropTypes.string,
};
