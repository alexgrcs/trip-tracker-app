import React from "react";
import PropTypes from "prop-types";
import Item from "./components/Item";
import { TripsWrapper, ErrorWrapper } from "./styles";
import { tripType } from "./types";
import Spinner from "./icons/Spinner";

function Trips({ trips, onItemClick, activeTripId, error, isFetching }) {
  const handleOnItemClick = (trip) => {
    return () => onItemClick(trip);
  };

  if (isFetching) {
    return (
      <TripsWrapper>
        <Spinner />
      </TripsWrapper>
    );
  }

  if (error) {
    return (
      <TripsWrapper>
        <ErrorWrapper>
          Sorry, an error has ocurred. :(
          <br />
          {error}
        </ErrorWrapper>
      </TripsWrapper>
    );
  }

  return (
    <TripsWrapper>
      <ul>
        {trips.map((trip) => (
          <Item
            key={`trip-${trip.id}`}
            onClick={handleOnItemClick(trip)}
            isActive={trip.id === activeTripId}
            {...trip}
          />
        ))}
      </ul>
    </TripsWrapper>
  );
}

Trips.propTypes = {
  trips: PropTypes.arrayOf(PropTypes.shape(tripType)),
  onItemClick: PropTypes.func,
  activeTripId: PropTypes.number,
  error: PropTypes.string,
  isFetching: PropTypes.bool,
};

Trips.defaultProps = {
  trips: [],
};

export default Trips;
