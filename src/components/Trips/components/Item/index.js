import React from "react";
import PropTypes from "prop-types";
import { formatDate, formatTime } from "../../../../utils/formatTimestamp";
import { getStartAndEndSpots, formatStatus, formatAddress } from "./utils";
import WheelIcon from "../../icons/WheelIcon";
import {
  ItemWrapper,
  RouteWrapper,
  AddressWrapper,
  TimeWrapper,
  DriverWrapper,
  StatusWrapper,
} from "./styles";
import { tripType } from "../../types";

function Item({
  description,
  driverName,
  startTime,
  endTime,
  origin,
  destination,
  status,
  onClick,
  isActive,
}) {
  const { start, end } = getStartAndEndSpots(description);

  return (
    <ItemWrapper onClick={onClick} isActive={isActive} data-testid="item">
      <TimeWrapper>
        <span>{formatDate(startTime)}</span>
      </TimeWrapper>
      <RouteWrapper>
        <span>{start}</span>
        <span className="separator" />
        <span>{end}</span>
      </RouteWrapper>
      <AddressWrapper>
        <span>{formatAddress(origin.address)}</span>
        <span>{formatAddress(destination.address)}</span>
      </AddressWrapper>
      <TimeWrapper>
        <span>{formatTime(startTime)}</span>
        <span>{formatTime(endTime)}</span>
      </TimeWrapper>
      <DriverWrapper>
        <WheelIcon isActive={isActive} />
        <span>{driverName}</span>
        <StatusWrapper status={status}>{formatStatus(status)}</StatusWrapper>
      </DriverWrapper>
    </ItemWrapper>
  );
}

Item.propTypes = {
  ...tripType,
  onClick: PropTypes.func,
  isActive: PropTypes.bool,
};

export default Item;
