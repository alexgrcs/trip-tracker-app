export function getStartAndEndSpots(description, separator = " a ") {
  let start = "";
  let end = "";

  const [startText, endText] = description.split(separator);

  if (startText) {
    start = startText;
  }

  if (endText) {
    end = endText;
  }

  return {
    start,
    end,
  };
}

export function formatStatus(status) {
  switch (status) {
    case "ongoing":
      return "En marcha";
    case "scheduled":
      return "Programado";
    case "finalized":
      return "Finalizado";
    case "cancelled":
      return "Cancelado";
    default:
      return "";
  }
}

const ONGOING_COLOR = "#ffb700";
const SCHEDULED_COLOR = "#357edd";
const FINALIZED_COLOR = "#19a974";
const CANCEL_COLOR = "#ff4136";
const DEFAULT_COLOR = "#dcdcdc";

export function getStatusColor(status) {
  switch (status) {
    case "ongoing":
      return ONGOING_COLOR;
    case "scheduled":
      return SCHEDULED_COLOR;
    case "finalized":
      return FINALIZED_COLOR;
    case "cancelled":
      return CANCEL_COLOR;
    default:
      return DEFAULT_COLOR;
  }
}

export function formatAddress(address) {
  return (address = address.substring(0, address.indexOf(",")));
}
