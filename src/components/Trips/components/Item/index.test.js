import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import Item from "./";
import mockTripData from "../../../../__mocks__/tripData";

afterEach(cleanup);

it("renders", () => {
  const mockIsActive = false;
  const mockOnClick = () => {};

  const { asFragment } = render(
    <Item {...mockTripData} isActive={mockIsActive} onClick={mockOnClick} />
  );
  expect(asFragment()).toMatchSnapshot();
});

it("click callback works", () => {
  const mockIsActive = false;
  const mockOnClick = jest.fn();

  const { getByTestId } = render(
    <Item {...mockTripData} isActive={mockIsActive} onClick={mockOnClick} />
  );

  fireEvent.click(getByTestId("item"));

  expect(mockOnClick.mock.calls.length).toEqual(1);
});
