import styled from "styled-components";
import { getStatusColor } from "./utils";

const SOFT_GRAY = "rgba(0, 0, 0, 0.275)";
const SOFTER_GRAY = "#dcdcdc";

const FlexBase = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ItemWrapper = styled.li`
  display: block;
  padding: 1em;
  background-color: ${(props) =>
    props.isActive ? "rgba(0, 0, 0, 0.065)" : "#ffffff"};
  border-bottom: 1px solid ${SOFTER_GRAY};
  cursor: pointer;
  transition: all 0.15s ease-out;

  &:hover {
    position: relative;
    z-index: 2;
    box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.15);
  }
`;

export const RouteWrapper = styled(FlexBase)`
  & > span {
    &.separator {
      display: flex;
      justify-content: center;
      align-items: center;

      &:after {
        display: block;
        content: "";
        position: relative;
        top: 2px;
        width: 80px;
        height: 1px;
        background-color: ${SOFTER_GRAY};
      }
    }
  }
`;

export const AddressWrapper = styled(FlexBase)`
  font-size: 0.75rem;
  color: ${SOFT_GRAY};

  & > span {
    &:nth-of-type(2) {
      text-align: right;
    }
  }
`;

export const TimeWrapper = styled(FlexBase)`
  color: ${SOFT_GRAY};
`;

export const DriverWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 0.85rem;
  font-weight: 500;
  color: ${SOFT_GRAY};
`;

export const StatusWrapper = styled.span`
  display: inline-block;
  font-size: 0.7rem;
  font-weight: bold;
  margin-top: 0.4em;
  color: ${(props) => getStatusColor(props.status)};
`;
