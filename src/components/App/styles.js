import styled from "styled-components";

export const AppWrapper = styled.div`
  display: flex;

  @media (max-width: 767px) {
    flex-direction: column-reverse;
  }
`;
