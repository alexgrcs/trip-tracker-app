import React, { useEffect, useState } from "react";
import { fetchTrips, fetchStop } from "../../api";
import Trips from "../Trips";
import Map from "../Map";
import Stop from "../Stop";
import { getQueryParams, updateTripParam } from "../../utils/queryParams";
import { AppWrapper } from "./styles";

function App() {
  const [isFetching, setIsFetching] = useState(false);
  const [trips, setTrips] = useState([]);
  const [errorTrips, setErrorTrips] = useState(null);
  const [activeTrip, setActiveTrip] = useState({});
  const [activeStopId, setActiveStopId] = useState(null);
  const [tooltip, setTooltip] = useState(null);

  useEffect(() => {
    const { tripId } = getQueryParams();

    async function getTrips() {
      setIsFetching(true);

      const { trips, error } = await fetchTrips();

      setIsFetching(false);

      if (error) {
        setErrorTrips(error);

        return;
      }

      setTrips(trips);

      if (tripId) {
        const tripIndex = tripId - 1;

        setActiveTrip(trips[tripIndex]);
      }
    }

    getTrips();
  }, []);

  const handleOnItemClick = (trip) => {
    setActiveTrip(trip);

    updateTripParam(trip.id);
  };

  const handleOnStopClick = (stopId) => {
    return async () => {
      if (activeStopId && activeStopId === stopId) {
        setActiveStopId(null);

        return;
      }

      setActiveStopId(stopId);
      setTooltip({ isFetching: true });

      const { stop, error } = await fetchStop(stopId);

      setTooltip({ stop, error, isFetching: false });
    };
  };

  const getTooltipData = (stopId) => {
    if (!tooltip || activeStopId !== stopId) {
      return;
    }

    return tooltip;
  };

  return (
    <AppWrapper>
      <Trips
        isFetching={isFetching}
        trips={trips}
        onItemClick={handleOnItemClick}
        activeTripId={activeTrip.id}
        error={errorTrips}
      />
      <Map
        route={activeTrip.route}
        origin={activeTrip.origin}
        destination={activeTrip.destination}
      >
        {activeTrip.stops &&
          activeTrip.stops.map((stop) => {
            return (
              <Stop
                key={`marker-${stop.id}`}
                onClick={handleOnStopClick(stop.id)}
                tooltip={getTooltipData(stop.id)}
                {...stop}
              />
            );
          })}
      </Map>
    </AppWrapper>
  );
}

export default App;
