import styled from "styled-components";

export const MapWrapper = styled.div`
  flex-basis: calc(100% - 300px);
  width: calc(100% - 300px);
  height: 100vh;

  @media (max-width: 767px) {
    flex-basis: 100%;
    width: 100%;
    height: 80vh;
  }
`;
