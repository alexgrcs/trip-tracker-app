import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import GoogleMap from "google-map-react";
import SpotIcon from "./icons/SpotIcon";
import { MapWrapper } from "./styles";
import { tripType } from "../Trips/types";

const GOOGLE_MAPS_API_KEY = process.env.GOOGLE_MAPS_API_KEY;

const DEFAULT_CENTER = {
  lat: 41.390205,
  lng: 2.154007,
};

const DEFAULT_ZOOM = 1;

function Map({ route, children, origin, destination }) {
  const [maps, setMaps] = useState(null);
  const [map, setMap] = useState(null);
  const [currentPath, setCurrentPath] = useState(null);

  function handleApiLoaded(map, maps) {
    setMap(map);
    setMaps(maps);

    setNewPath();
  }

  const zoomToPath = (path) => {
    const bounds = new maps.LatLngBounds();
    const points = path.getPath().getArray();

    for (var n = 0; n < points.length; n++) {
      bounds.extend(points[n]);
    }

    map.fitBounds(bounds);
  };

  const setNewPath = () => {
    if (!route || !map || !maps) {
      return;
    }

    const path = maps.geometry.encoding.decodePath(route);

    if (currentPath) {
      currentPath.setMap(null);
    }

    const newPath = new maps.Polyline({
      path,
      strokeColor: "#ff41b4",
      strokeOpacity: 1.0,
      strokeWeight: 3,
    });

    setCurrentPath(newPath);

    newPath.setMap(map);
    zoomToPath(newPath);
  };

  useEffect(() => {
    setNewPath();
  }, [route, map, maps]);

  return (
    <MapWrapper>
      <GoogleMap
        bootstrapURLKeys={{ key: GOOGLE_MAPS_API_KEY, libraries: ["geometry"] }}
        defaultCenter={DEFAULT_CENTER}
        defaultZoom={DEFAULT_ZOOM}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
      >
        {origin && <SpotIcon lat={origin.lat} lng={origin.lng} />}
        {destination && (
          <SpotIcon filled lat={destination.lat} lng={destination.lng} />
        )}
        {children}
      </GoogleMap>
    </MapWrapper>
  );
}

const { route, origin, destination } = tripType;

Map.propTypes = {
  route,
  origin,
  destination,
  children: PropTypes.node,
};

export default Map;
