import React from "react";
import PropTypes from "prop-types";
import { SvgWrapper, IconWrapper } from "./styles";

function SpotIcon({ filled }) {
  return (
    <IconWrapper>
      <SvgWrapper viewBox="0 0 32 32">
        {filled ? (
          <path d="M16 0c-5.523 0-10 4.477-10 10 0 10 10 22 10 22s10-12 10-22c0-5.523-4.477-10-10-10zM16 16.125c-3.383 0-6.125-2.742-6.125-6.125s2.742-6.125 6.125-6.125 6.125 2.742 6.125 6.125-2.742 6.125-6.125 6.125zM12.125 10c0-2.14 1.735-3.875 3.875-3.875s3.875 1.735 3.875 3.875c0 2.14-1.735 3.875-3.875 3.875s-3.875-1.735-3.875-3.875z" />
        ) : (
          <path d="M16 0c-5.523 0-10 4.477-10 10 0 10 10 22 10 22s10-12 10-22c0-5.523-4.477-10-10-10zM16 16c-3.314 0-6-2.686-6-6s2.686-6 6-6 6 2.686 6 6-2.686 6-6 6z" />
        )}
      </SvgWrapper>
    </IconWrapper>
  );
}

SpotIcon.propTypes = {
  filled: PropTypes.bool,
};

export default SpotIcon;
