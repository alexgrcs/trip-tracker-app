import styled from "styled-components";

export const IconWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 36px;
  height: 36px;
  transform: translate(-18px, -18px);
  background-color: #ffffff;
  border-radius: 50%;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25);
`;

export const SvgWrapper = styled.svg`
  display: block;
  width: 22px;
  height: 22px;
  fill: #5e2ca5;
`;
