import { parseTripsData } from "./utils/parseTrips";
import { parseStopData } from "./utils/parseStop";

const API_BASE_URL = process.env.API_BASE_URL;

export function fetchTrips() {
  return fetch(`${API_BASE_URL}trips`)
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      return { error: error.message };
    })
    .then((json) => {
      return parseTripsData(json);
    });
}

export function fetchStop(id) {
  return fetch(`${API_BASE_URL}stops/${id}`)
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      return { error: error.message };
    })
    .then((json) => {
      return parseStopData(json);
    });
}
