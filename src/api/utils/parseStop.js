export function parseStopData(data) {
  /**
   * Error handling
   * { error }
   */
  if (!data || data.error) {
    return { error: "No data available" };
  }

  if (data.error) {
    return data;
  }

  return { stop: data };
}
