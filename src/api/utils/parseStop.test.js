import { parseStopData } from "./parseStop";
import stopData from "../../__mocks__/stopData";

it("returns the parsed success data", () => {
  const mockData = stopData;
  const expectedData = { stop: stopData };

  const parsedData = parseStopData(mockData);

  expect(parsedData).toStrictEqual(expectedData);
});

it("returns the parsed error data", () => {
  const mockErrorData = { error: "A message" };
  const expectedData = { error: "No data available" };

  const parsedData = parseStopData(mockErrorData);

  expect(parsedData).toStrictEqual(expectedData);
});

it("returns the error data if there is no input", () => {
  const expectedData = { error: "No data available" };

  const parsedData = parseStopData();

  expect(parsedData).toStrictEqual(expectedData);
});
