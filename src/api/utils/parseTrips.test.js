import { parseTripsData } from "./parseTrips";
import tripsResponse from "../../__mocks__/tripsResponse";
import tripsData from "../../__mocks__/tripsData";

it("returns the parsed success data", () => {
  const mockData = tripsResponse;
  const expectedData = { trips: tripsData };

  const parsedData = parseTripsData(mockData);

  expect(parsedData).toStrictEqual(expectedData);
});

it("returns the parsed error data", () => {
  const mockErrorData = { error: "An error message" };
  const expectedData = mockErrorData;

  const parsedData = parseTripsData(mockErrorData);

  expect(parsedData).toStrictEqual(expectedData);
});

it("returns the error data if there is no input", () => {
  const expectedData = { error: "An error has ocurred." };

  const parsedData = parseTripsData();

  expect(parsedData).toStrictEqual(expectedData);
});
