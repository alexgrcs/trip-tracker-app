export function parseTripsData(data) {
  /**
   * Error handling
   * { error }
   */
  if (!data) {
    return { error: "An error has ocurred." };
  }

  if (!Array.isArray(data)) {
    return data;
  }

  return mapTripsData(data);
}

function mapTripsData(data) {
  const sortedTrips = data.sort((a, b) => {
    return new Date(a.startTime) - new Date(b.startTime);
  });

  const trips = sortedTrips.map((trip, index) => {
    return mapTripData(trip, index);
  });

  return { trips };
}

function mapTripData(trip, index) {
  const { origin, destination, stops } = mapCoordinates(trip);

  const tripData = { ...trip, id: index + 1, origin, destination, stops };

  return tripData;
}

function mapCoordinates(trip) {
  const { origin, destination, stops } = trip;

  return {
    origin: mapCoordinateData(origin),
    destination: mapCoordinateData(destination),
    stops: mapStopsCoordinates(stops),
  };
}

function mapCoordinateData(data) {
  return {
    address: data.address,
    lat: data.point._latitude,
    lng: data.point._longitude,
  };
}

function mapStopsCoordinates(stops) {
  /**
   * When the trip doesn't have any stop
   * the API returns an array with one empty item.
   */
  if (!stops[0].id) {
    return [];
  }

  return stops.map(({ id, point }) => {
    return {
      id,
      lat: point._latitude,
      lng: point._longitude,
    };
  });
}
