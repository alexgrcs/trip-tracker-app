export default {
  origin: {
    address: "Metropolis:lab, Barcelona",
    lat: 41.38074,
    lng: 2.18594,
  },
  stops: [
    {
      id: 7,
      lat: 41.42658,
      lng: 2.22688,
    },
    {
      id: 8,
      lat: 41.48216,
      lng: 2.17263,
    },
  ],
  destination: {
    address: "Cerdanyola Centre, El Valles",
    lat: 41.4914,
    lng: 2.13349,
  },
  endTime: "2018-12-18T07:50:00.000Z",
  startTime: "2018-12-18T07:00:00.000Z",
  description: "Barcelona a Cerdanyola",
  driverName: "Rosalia",
  route: "sdq{Fc}iLurAy{AyiEabDgfDhqEgyB|gA{x@xv@{x@rsF",
  status: "ongoing",
  id: 2,
};
