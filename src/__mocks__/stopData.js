export default {
  point: {
    _latitude: 42.07967,
    _longitude: 2.81734,
  },
  price: 2.5,
  stopTime: "2018-12-24T16:00:00.000Z",
  paid: false,
  address: "Cornella del Terri",
  tripId: 4,
  userName: "Laganja Greene",
};
