module.exports = {
  extends: [
    "standard",
    "standard-react",
    "prettier",
    "prettier/standard",
    "prettier/react",
  ],
  parserOptions: {
    ecmaVersion: 9,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  plugins: ["html"],
};
