# Trip Tracker App

You can see the project at [https://trip-tracker-d837a4.netlify.app/](https://trip-tracker-d837a4.netlify.app/).

## Description

This is a simple SPA app built in React, using Webpack as a module bundler along with Babel, which makes it possible to work with the last ECMAScript features.

The components architecture is organised with a “domain driven" mindset, trying to follow the business logic and keeping the different responsabilities of my application separated from the others, which makes it easier to test and scale. Domains are organized by subdirectories inside the `/components` folder, which contains all the files involving that domain ("container", components, states, tests, styles, etc.).

I usually use Redux (or useReducer) as the main state manager in almost every large application I’ve worked on lately. For this project I decided not to overengineer using Redux, since there is very little component nesting, but I kept most of the state centralised in the `<App />` component. This way I can keep most of the components totally functional.

For styling I used styled-components, which makes really easy to keep the scope of the styles inside the component. I also created some base global styles (let's not lose the "cascading" feature of CSS completely!).

I also used the <a href="https://github.com/google-map-react/google-map-react/" target="_blank">Google Map React</a> library for this particular case. I try to avoid external libraries that I don't need, but I found that this one fits very well with the purpose of this test. Also it is quite popular and well supported by their maintainers.

## What would I add if I had more time

- A more robust routing.
- Some more meaningful unit tests.
- E2E testing some usecases, using TestCafe for example.
- Better responsiveness for mobile.
- Probably at this point the state is becoming quite complex at the `<App />` component, so it could be a good moment to switch to useReducer and useContext for state management.

## Main Available Scripts

In the project directory, you can run:

### `npm install`

First of all, install the project dependencies! :)

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.

### `npm test`

Launches the test runner.

### `npm run build`

Builds the app for production to the `dist` folder.
